const initialState = {
 cart: [
  {
   id: 1,
   name: "Blue denim shirt",
   price: 10,
   image: "blue.png",
   type: "Shirt",
   size: "M",
   quantity: 0,
  },
  {
   id: 2,
   name: "Red hoodie",
   price: 20,
   image: "red.png",
   type: "Jacket",
   size: "XL",
   quantity: 0,
  },
 ],
};

const reducer = (state = initialState, action) => {
 switch (action.type) {
  case "ADD_TO_CART":
   const existingItemIndex = state.cart.findIndex(
    (item) => item.id === action.payload.id
   );

   if (existingItemIndex !== -1) {
    const updatedCart = [...state.cart];
    updatedCart[existingItemIndex].quantity += 1;

    return {
     ...state,
     cart: updatedCart,
    };
   } else {
    const newItem = { ...action.payload, quantity: 1 };
    return {
     ...state,
     cart: [...state.cart, newItem],
    };
   }

  case "REMOVE_FROM_CART":
   return {
    ...state,
    cart: state.cart.filter((item) => item.id !== action.payload),
   };

  case "REDUCE_FROM_CART":
   const itemIndex = state.cart.findIndex(
    (item) => item.id === action.payload.id
   );

   if (itemIndex !== -1) {
    const updatedCart = [...state.cart];
    if (updatedCart[itemIndex].quantity > 0) {
     updatedCart[itemIndex].quantity -= 1;
    }

    return {
     ...state,
     cart: updatedCart,
    };
   } else {
    const newItem = { ...action.payload, quantity: 1 };
    return {
     ...state,
     cart: [...state.cart, newItem],
    };
   }
  default:
   return state;
 }
};

export default reducer;
