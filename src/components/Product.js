const Product = ({ product, addToCart, removeFromCart, reduceFromCart }) => {
 return (
   <div className="border flex flex-col md:flex-row gap-4 justify-center items-center py-8 lg:py-0">
     <img
       src={require(`../images/${product.image}`)}
       alt={product.name}
       className="w-48 h-48 object-contain"
     />
     <div className="flex flex-col lg:justify-between w-full justify-center lg:items-stretch items-center">
       <div className="flex flex-col md:flex-row items-center md:justify-between lg:mr-2">
         <p className="text-xl font-semibold">{product.name}</p>
         <div className="flex items-center gap-2 md:gap-10 bg-slate-100">
           <button
             className="px-2 rounded hover:bg-slate-200 bg-slate-300"
             onClick={() => reduceFromCart(product)}
           >
             -
           </button>
           <p className="bg-blue-100 px-2 rounded">{product.quantity || 0}</p>
           <button
             className="px-2 rounded hover:bg-slate-200 bg-slate-300"
             onClick={() => addToCart(product)}
           >
             +
           </button>
         </div>
       </div>
       <p className="text-md antialiased">Type: {product.type}</p>
       <p className="text-md antialiased">Size: {product.size}</p>
       <div className="flex flex-col md:flex-row md:items-center justify-between py-8">
         <div className="flex flex-col md:flex-row gap-3 items-center">
           <p
             className="text-gray-400 text-sm cursor-pointer hover:underline uppercase"
             onClick={() => removeFromCart(product.id)}
           >
             🗑️ remove item
           </p>
           <p className="text-gray-400 text-sm cursor-pointer hover:underline uppercase">
             ❤️️ move to wish list
           </p>
         </div>
         <p className="text-gray-700 mt-2 md:mt-0 text-center lg:text-left lg:mr-3">${product.price}</p>
       </div>
     </div>
   </div>
 );
};

export default Product;
