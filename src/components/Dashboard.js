import React from "react";
import { useSelector, useDispatch } from 'react-redux';
import Product from './Product';
import Cart from './Cart';

const Dashboard = () => {
 const dispatch = useDispatch();
 const cartItems = useSelector((state) => state.cart);

 const addToCart = (product) => {
  dispatch({ type: "ADD_TO_CART", payload: product });
 };

 const removeFromCart = (productId) => {
  dispatch({ type: "REMOVE_FROM_CART", payload: productId });
 };

 const reduceFromCart = (product) => {
  dispatch({ type: "REDUCE_FROM_CART", payload: product });
 };
 return (
  <div>
   <h1 className="text-2xl bg-gray-100 font-bold text-center py-3">
    Shopping cart
   </h1>
   <h1 className="text-lg font-bold antialiased lg:ml-[7rem] mt-4 text-center lg:text-left">
    Cart ({cartItems.length} items)
   </h1>
   <div className="grid lg:flex gap-2 justify-center my-5">
    <div className="w-full lg:w-1/2">
     {cartItems.map((product) => (
      <Product
       key={product.id}
       product={product}
       addToCart={addToCart}
       removeFromCart={removeFromCart}
       reduceFromCart={reduceFromCart}
      />
     ))}
    </div>
    <div className="w-full lg:w-1/3">
     <Cart cartItems={cartItems} />
    </div>
   </div>
  </div>
 );
};

export default Dashboard;
