import { useState } from 'react';

const Collapse = ({ title }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleCollapse = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="border p-2">
      <div
        className="flex items-center justify-between cursor-pointer"
        onClick={toggleCollapse}
      >
        <h3 className="text-gray-400">{title}</h3>
        <svg
          className={`w-6 h-6 transition-transform transform ${
            isOpen ? 'rotate-180' : 'rotate-0'
          }`}
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d={
              isOpen
                ? 'M19 9l-7 7-7-7'
                : 'M9 5l7 7-7 7'
            }
          />
        </svg>
      </div>
      {isOpen && (
        <input className="border rounded py-1 px-2 mt-2 w-full"/>
      )}
    </div>
  );
};

export default Collapse;
