const CheckoutSuccess = () => {
  return (
    <div className="container mx-auto p-4">
      <h2 className="text-2xl font-semibold mb-4">Payment Successful</h2>
      <div className="bg-green-100 p-4 rounded">
        <p className="text-green-700 text-lg">
          Your payment has been successfully processed. Thank you for your order!
        </p>
      </div>
    </div>
  );
};

export default CheckoutSuccess;
