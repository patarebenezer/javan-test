import CartItem from './CartItem';

const Cart = ({ cartItems }) => {
  const totalItems = cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
  return (
    <div className="shadow-lg rounded-lg p-4 m-2">
      {cartItems.length === 0 ? (
        <p>Your cart is empty.</p>
      ) : (
        <ul>
            <CartItem item={totalItems}/>
        </ul>
      )}
    </div>
  );
};

export default Cart;
