import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router';

const Checkout = () => {
  const navigate = useNavigate()
  const { item } = useParams();
  const [formData, setFormData] = useState({
    name: 'Patar Siahaan',
    cardNumber: '',
    total: item
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleCheckout = () => {
    navigate('/checkout-success');
  };

  return (
    <div className="container lg:w-1/2 mx-auto p-4">
      <h2 className="text-2xl font-semibold mb-4">Checkout</h2>
      <form className="border p-4">
        <h3 className="text-lg font-semibold mb-2">Billing Information</h3>
        <div className="mb-2">
          <label className="block">Name</label>
          <input
            type="text"
            name="name"
            value={formData.name}
            onChange={handleInputChange}
            className="w-full border p-2"
            required
          />
        </div>

        <h3 className="text-lg font-semibold mb-2">Payment Information</h3>
        <div className="mb-2">
          <label className="block">Card Number</label>
          <input
            type="text"
            name="cardNumber"
            value={formData.cardNumber}
            onChange={handleInputChange}
            className="w-full border p-2"
            required
          />
        </div>
        
        <div className="mb-2">
          <label className="block">Total</label>
          <input
            type="text"
            name="cardNumber"
            prefix='$'
            value={formData.total}
            onChange={handleInputChange}
            className="w-full border p-2"
            required
          />
        </div>
        <button
          type="button"
          onClick={handleCheckout}
          className="bg-blue-500 text-white px-4 py-2 rounded w-full"
        >
          Pay
        </button>
      </form>
    </div>
  );
};

export default Checkout;
