import Collapse from "./Collapse";
import { useNavigate } from 'react-router-dom';

const CartItem = ({ item }) => {
  const navigate = useNavigate();
 return (
  <div className="p-4">
   <h1 className="font-bold antialiased text-xl pb-5">The total amount of </h1>
   <div className="flex justify-between py-3">
    <p>Temporary amount</p>
    <p>{item}</p>
   </div>
   <div className="flex justify-between py-3">
    <p>Shipping</p>
    <p>Free</p>
   </div>
   <hr />
   <div className="flex justify-between py-3">
    <p className="font-bold antialiased">The total amount of (including VAT)</p>
    <p>{item}</p>
   </div>
   <button
    className="bg-blue-500 rounded-sm w-full py-2 my-4 text-white hover:bg-blue-400"
    onClick={() => navigate(`/checkout/${item}`)}
   >
    Go to checkout
   </button>
   <Collapse title={"Add a discount code [optional]"} />
  </div>
 );
};

export default CartItem;
